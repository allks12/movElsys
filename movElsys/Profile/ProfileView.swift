//
//  ProfileView.swift
//  movElsys
//
//  Created by Aleks on 18.12.22.
//

import SwiftUI
import FirebaseAuth

struct ProfileView: View {
    @Binding var isLoggedIn: Bool
    @EnvironmentObject private var viewModel: UserViewModel
    @EnvironmentObject private var goalsViewModel: GoalsViewModel
    
    private var currDailyStepGoal: Int {
        goalsViewModel.getStepsGoal()
    }
    
    private var currDailyEnergyGoal: Int {
        goalsViewModel.getEnergyGoal()
    }
    
    var body: some View {
        VStack {
            Text("My profile")
                .font(.largeTitle)
                .fontWeight(.bold)
                .foregroundColor(Color.black)
                .padding(.vertical, 50)
            
            Image(systemName: "person.circle")
                .font(.system(size: 90))
                .foregroundColor(Color(R.color.darkBlue))
            
            HStack {
                displayNameOrErrorMessage()
            }
            .font(.title2)
            .padding(.vertical)
            
            GoalsView(stepsGoal: currDailyStepGoal,
                      energyGoal: currDailyEnergyGoal)
            
            Spacer()
            
            Button {
                viewModel.logout { success in
                    switch success {
                    case .success(_):
                        isLoggedIn = false
                    case .failure(_):
                        isLoggedIn = true
                    }
                }
            } label: {
                ButtonView(text: "Logout")
                    .foregroundColor(Color(R.color.darkBlue))
            }
        }
    }
    
    func displayNameOrErrorMessage() -> Text {
        var stringForDisplay = ""
        var getNameSuccess = true
        viewModel.getUserName { success in
            switch success {
            case .success(let name):
                getNameSuccess = true
                stringForDisplay = name
            case .failure(_):
                getNameSuccess = false
                stringForDisplay = ErrorMessage.unableUserName.rawValue
            }
        }
        
        return Text(stringForDisplay)
            .foregroundColor(getNameSuccess ? Color(R.color.darkBlue) : .red)
    }
}
