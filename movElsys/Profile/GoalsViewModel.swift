//
//  GoalsViewModel.swift
//  movElsys
//
//  Created by Aleksandra on 5.02.23.
//

import Foundation

class GoalsViewModel: ObservableObject {
    private let defaults = UserDefaults.standard
    private let stepsKeyName = "stepsDailyGoal"
    private let energyKeyName = "energyDailyGoal"
    
    init() {
        if defaults.object(forKey: stepsKeyName) == nil {
            defaults.set(10000, forKey: stepsKeyName)
        }
        
        if defaults.object(forKey: energyKeyName) == nil {
            defaults.set(230, forKey: energyKeyName)
        }
    }
    
    func updateStepsDailyGoal(newGoal: Int) {
        if newGoal > 0 {
            defaults.set(newGoal, forKey: stepsKeyName)
            defaults.synchronize()
        }
    }
    
    func updateEnergyDailyGoal(newGoal: Int) {
        if newGoal > 0 {
            defaults.set(newGoal, forKey: energyKeyName)
            defaults.synchronize()
        }
    }
    
    func getStepsGoal() -> Int {
        defaults.integer(forKey: stepsKeyName)
    }
    
    func getEnergyGoal() -> Int {
        defaults.integer(forKey: energyKeyName)
    }
}
