//
//  ProfileSettingsView.swift
//  movElsys
//
//  Created by Aleksandra on 5.02.23.
//

import SwiftUI

struct ProfileSettingsView: View {
    @State private var newName = ""
    @State private var newEmail = ""
    @State private var currPasswordEmailChange = ""
    @State private var newPassword = ""
    @State private var newPassword2 = ""
    @State private var newStepsGoal = ""
    @State private var newEnergyGoal = ""
    @State private var currPassPasswordChange = ""
    
    @State private var updateEmailSuccess = true
    @State private var updatePasswordSuccess = true
    @State private var passwordErrorMessage = ""
    @State private var updateNameSuccess = true
    
    @EnvironmentObject private var goalsViewModel: GoalsViewModel
    @EnvironmentObject private var userViewModel: UserViewModel
    
    private let fieldValidator = FieldValidator()
    
    var body: some View {
        ScrollView {
            VStack {
                Text("Profile settings")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundColor(Color.black)
                    .padding(.bottom)
                
                HeadlinePositionedRightView(text: "Change daily goals")
                
                HStack {
                    ChangeSettingsFieldView(content: $newStepsGoal,
                                            placeholder: "Enter new steps goal",
                                            isItNum: true)
                    
                    Button {
                        if let stepsGoal = Int(newStepsGoal) {
                            goalsViewModel.updateStepsDailyGoal(newGoal: stepsGoal)
                            newStepsGoal = ""
                        }
                        hideKeyboard()
                    } label: {
                        Image(systemName: "arrow.right")
                            .imageScale(.large)
                    }
                    .disabled(newStepsGoal.isEmpty)
                }
                .padding(.bottom)
                
                HStack {
                    ChangeSettingsFieldView(content: $newEnergyGoal,
                                            placeholder: "Enter new burned energy goal",
                                            isItNum: true)
                    
                    Button {
                        if let energyGoal = Int(newEnergyGoal) {
                            goalsViewModel.updateEnergyDailyGoal(newGoal: energyGoal)
                            newEnergyGoal = ""
                        }
                        hideKeyboard()
                    } label: {
                        Image(systemName: "arrow.right")
                            .imageScale(.large)
                    }
                    .disabled(newEnergyGoal.isEmpty)
                    
                }
                .padding(.bottom)
                
                VStack {
                    
                    HeadlinePositionedRightView(text: "Change name")
                    
                    HStack {
                        ChangeSettingsFieldView(content: $newName,
                                                placeholder: "Enter new name",
                                                isItNum: false)
                        
                        Button {
                            userViewModel.updateUserName(userName: newName) { success in
                                switch success {
                                case .success():
                                    updateNameSuccess = true
                                case .failure(_):
                                    updateNameSuccess = false
                                }
                            }
                            newName = ""
                        } label: {
                            Image(systemName: "arrow.right")
                                .imageScale(.large)
                        }
                        .disabled(!fieldValidator.validateName(newName))
                    }
                    
                    if !updateNameSuccess {
                        Text(ErrorMessage.unsuccessfullChange.rawValue)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                }
                .padding(.bottom)
                
                VStack {
                    HeadlinePositionedRightView(text: "Change email")
                    
                    HStack {
                        VStack {
                            ChangeSettingsFieldView(content: $newEmail,
                                                    placeholder: "Enter new email",
                                                    isItNum: false)
                            .padding(.bottom)
                            
                            ChangeSettingsPasswordFieldView(content: $currPasswordEmailChange,
                                                            placeholder: "Enter your current password")
                        }
                        
                        Button {
                            userViewModel.updateEmail(newEmail: newEmail,
                                                      password: currPasswordEmailChange) { success in
                                switch success {
                                case .success():
                                    updateEmailSuccess = true
                                case .failure(_):
                                    updateEmailSuccess = false
                                }
                            }
                            newEmail = ""
                            currPasswordEmailChange = ""
                            hideKeyboard()
                        } label: {
                            Image(systemName: "arrow.right")
                                .imageScale(.large)
                        }
                        .disabled(!fieldValidator.validateEmail(newEmail)
                                  || !fieldValidator.validatePassword(currPasswordEmailChange))
                    }
                    
                    if !updateEmailSuccess {
                        Text(ErrorMessage.unsuccessfullChange.rawValue)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                }
                .padding(.bottom)
                
                VStack {
                    HeadlinePositionedRightView(text: "Change password")
                    
                    HStack {
                        VStack {
                            ChangeSettingsPasswordFieldView(content: $currPassPasswordChange,
                                                            placeholder: "Enter your current password")
                            .padding(.bottom)
                            
                            ChangeSettingsPasswordFieldView(content: $newPassword,
                                                            placeholder: "Enter new password")
                            .padding(.bottom)
                            
                            ChangeSettingsPasswordFieldView(content: $newPassword2,
                                                            placeholder: "Repeat new password")
                        }
                        
                        Button {
                            if !fieldValidator.areTwoPasswordsTheSame(pass1: newPassword,
                                                                      pass2: newPassword2) {
                                updatePasswordSuccess = false
                                passwordErrorMessage = ErrorMessage.passwordsNotMatch.rawValue
                            } else {
                                userViewModel.updatePassword(newPassword: newPassword,
                                                             currPassword: currPassPasswordChange) { success in
                                    switch success {
                                    case .success():
                                        updatePasswordSuccess = true
                                    case .failure(_):
                                        updatePasswordSuccess = false
                                        passwordErrorMessage = ErrorMessage.unsuccessfullChange.rawValue
                                    }
                                }
                            }
                            newPassword = ""
                            newPassword2 = ""
                            currPassPasswordChange = ""
                            hideKeyboard()
                        } label: {
                            Image(systemName: "arrow.right")
                                .imageScale(.large)
                        }
                        .disabled(!fieldValidator.validatePassword(newPassword)
                                  || !fieldValidator.validatePassword(newPassword2)
                                  || !fieldValidator.validatePassword(currPassPasswordChange))
                    }
                    
                    if !updatePasswordSuccess {
                        Text(passwordErrorMessage)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                }
                .padding(.bottom)
            }
            .padding(.horizontal)
            .onAppear {
                updateEmailSuccess = true
                updatePasswordSuccess = true
            }
            .onTapGesture {
                hideKeyboard()
            }
        }
    }
}

struct ProfileSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileSettingsView()
    }
}
