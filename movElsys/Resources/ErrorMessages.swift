//
//  ErrorMessages.swift
//  movElsys
//
//  Created by Aleks on 5.01.23.
//

import Foundation

enum ErrorMessage: String {
    case invalidEmail = "Email is not valid!"
    case tooShortPass = "Password must be at least 6 characters!"
    case wrongEmailOrPass = "Email or password is wrong!"
    case takenEmail = "This email is already registered!"
    case passwordsNotMatch = "Passwords are not the same!"
    case healthDataAuthFailed = "Authorizing access to health data wasn't successfull!"
    case dailyDataError = "Couldn't get the daily steps or energy!"
    case weekMonthStepsError = "Couldn't get the weekly ot monthly steps!"
    case historyStepsError = "Couldn't load steps for history!"
    case historyEnergyError = "Couldn't load burned energy for history!"
    case unsuccessfullChange = "Unsuccessfull change!"
    case unableUserName = "Enable to dislay your name!"
}

enum Errors: Error {
    case loginError
    case registerError
    case unathorizedHealthDataAccess
    case burnedEnergyError
    case monthStepsError
    case weekStepsError
    case dailyStepsError
    case historyStepsError
    case historyEnergyError
    case updateEmailError
    case updatePassword
    case passwordsDontMatch
    case updateUserNameError
    case getUserNameError
}
