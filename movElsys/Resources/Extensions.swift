//
//  Extensions.swift
//  movElsys
//
//  Created by Aleksandra on 27.01.23.
//

import Foundation
import SwiftUI

#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif

extension Date {
    static var now: Date {
        return Date()
    }
}
