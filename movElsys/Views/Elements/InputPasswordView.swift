//
//  InputPasswordView.swift
//  movElsys
//
//  Created by Aleks on 5.01.23.
//

import SwiftUI

struct InputPasswordView: View {
    @Binding private var content: String
    private let placeholder: String
    
    public init(content: Binding<String>, placeholder: String) {
        self._content = content
        self.placeholder = placeholder
    }
    
    var body: some View {
        SecureField(placeholder, text: $content)
            .padding(.all)
            .background(Color(.secondarySystemBackground))
            .cornerRadius(5.0)
            .disableAutocorrection(true)
            .autocapitalization(.none)
    }
}

struct InputPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        InputPasswordView(content: .constant(""), placeholder: "Password")
    }
}
