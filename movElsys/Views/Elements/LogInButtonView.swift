//
//  LoginButtonView.swift
//  movElsys
//
//  Created by Aleks on 23.12.22.
//

import SwiftUI

struct ButtonView: View {
    private var text: String
    
    public init(text: String) {
        self.text = text
    }
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10.0)
                .aspectRatio(7/1, contentMode: .fit)
            
            Text(text)
                .font(.title)
                .fontWeight(.bold)
                .foregroundColor(.white)
        }
        .padding(.all)
    }
}

struct LoginButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView(text: "Login")
    }
}
