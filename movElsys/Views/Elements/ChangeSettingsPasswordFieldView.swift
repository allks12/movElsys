//
//  ChangeSettingsPasswordFieldView.swift
//  movElsys
//
//  Created by Aleksandra on 5.02.23.
//

import SwiftUI

struct ChangeSettingsPasswordFieldView: View {
    @Binding private var content: String
    private let placeholder: String
        
    public init(content: Binding<String>, placeholder: String) {
        self._content = content
        self.placeholder = placeholder
    }
    
    var body: some View {
        SecureField(placeholder, text: $content)
                .padding(.all)
                .background(Color(.secondarySystemBackground))
                .cornerRadius(5.0)
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .foregroundColor(Color(R.color.darkBlue))
    }
}

struct ChangeSettingsPasswordFieldView_Previews: PreviewProvider {
    static var previews: some View {
        ChangeSettingsPasswordFieldView(content: .constant(""), placeholder: "Text")
    }
}
