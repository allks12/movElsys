//
//  InputTextFieldView.swift
//  movElsys
//
//  Created by Aleks on 5.01.23.
//

import SwiftUI

struct InputTextFieldView: View {
    @Binding private var content: String
    private let placeholder: String
    
    public init(content: Binding<String>, placeholder: String) {
        self._content = content
        self.placeholder = placeholder
    }
    
    var body: some View {
        TextField(placeholder, text: $content)
            .padding(.all)
            .background(Color(.secondarySystemBackground))
            .cornerRadius(5.0)
            .disableAutocorrection(true)
            .autocapitalization(.none)
    }
}

struct InputTextFieldView_Previews: PreviewProvider {
    static var previews: some View {
        InputTextFieldView(content: .constant(""), placeholder: "Text")
    }
}
