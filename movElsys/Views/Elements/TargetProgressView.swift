//
//  TargetProgressView.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import SwiftUI

struct TargetProgressView: View {
    private var currValue: Float
    private var goal: Float
    private let lineWidth = 7
    
    private var progress: CGFloat {
        CGFloat(currValue/goal)
    }
    
    private var color: Color {
        if progress <= 1/4 { return Color.red }
        if progress <= 1/2 { return Color.orange }
        if progress <= 3/4 { return Color.yellow }
        return Color.green
    }
    
    init(currValue: Int, goal: Int) {
        self.currValue = Float(currValue)
        self.goal = Float(goal)
    }
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(color.opacity(0.5),
                        lineWidth: CGFloat(lineWidth))
            Circle()
                .trim(from: 0,
                      to: progress)
                .stroke(color,
                        style: StrokeStyle(
                            lineWidth: CGFloat(lineWidth),
                            lineCap: .butt))
                .rotationEffect(.degrees(-90))
        }
        .frame(width: 15, height: 15)
    }
}

struct TargetProgressView_Previews: PreviewProvider {
    static var previews: some View {
        TargetProgressView(currValue: 10, goal: 30)
    }
}
