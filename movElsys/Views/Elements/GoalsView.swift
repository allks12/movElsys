//
//  GoalsView.swift
//  movElsys
//
//  Created by Aleksandra on 5.02.23.
//

import SwiftUI

struct GoalsView: View {
    @State var stepsGoal: String
    @State var energyGoal: String
    
    init(stepsGoal: Int, energyGoal: Int) {
        self.stepsGoal = String(stepsGoal)
        self.energyGoal = String(energyGoal)
    }
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 20)
                .fill(.blue.opacity(0.2))
                .frame(width: UIScreen.main.bounds.width - 20, height: 150)
                .padding(.horizontal)
            
            VStack {
                Text("Daily goals")
                    .font(.title3)
                    .fontWeight(.bold)
                    .padding(.bottom)
                
                HStack {
                    VStack {
                        Image(systemName: "shoeprints.fill")
                            .foregroundColor(Color(R.color.darkBlue))
                            .padding(.bottom, 3)
                        Image(systemName: "flame.fill")
                            .foregroundColor(.pink)
                    }
                    
                    VStack {
                        Text(stepsGoal)
                            .foregroundColor(Color(R.color.darkBlue))
                            .padding(.bottom, 1)
                        
                        Text(energyGoal)
                            .foregroundColor(.pink)
                    }
                }
                
            }
        }
    }
}

struct GoalsView_Previews: PreviewProvider {
    static var previews: some View {
        GoalsView(stepsGoal: 10000, energyGoal: 500)
    }
}
