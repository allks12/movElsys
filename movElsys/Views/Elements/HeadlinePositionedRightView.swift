//
//  HeadlinePositionedRightView.swift
//  movElsys
//
//  Created by Aleksandra on 5.02.23.
//

import SwiftUI

struct HeadlinePositionedRightView: View {
    @State var text: String
    
    init(text: String) {
        self.text = text
    }
    
    var body: some View {
        HStack {
            Text(text)
                .font(.title)
                .fontWeight(.bold)
                .foregroundColor(Color.black)
                .padding(.vertical)
            
            Spacer()
        }
    }
}

struct HeadlinePositionedRightView_Previews: PreviewProvider {
    static var previews: some View {
        HeadlinePositionedRightView(text: "Headline")
    }
}
