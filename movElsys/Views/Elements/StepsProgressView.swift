//
//  SwiftUIView.swift
//  movElsys
//
//  Created by Aleksandra on 29.01.23.
//

import SwiftUI

struct ProgressView: View {
    private var currValue: Float
    private var goal: Float
    private var frameWidth: Int
    private var frameHeight: Int
    private var lineWidth: Int
    private var color: Color
    private var progress: CGFloat {
        CGFloat(currValue/goal)
    }
    
    init(currValue: Int,
         goal: Int,
         frameWidth: Int,
         frameHeight: Int,
         lineWidth: Int,
         color: Color) {
        self.currValue = Float(currValue)
        self.goal = Float(goal)
        self.frameWidth = frameWidth
        self.frameHeight = frameHeight
        self.lineWidth = lineWidth
        self.color = color
    }
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(color.opacity(0.5),
                        lineWidth: CGFloat(lineWidth))
            Circle()
                .trim(from: 0,
                      to: progress)
                .stroke(color,
                        style: StrokeStyle(
                            lineWidth: CGFloat(lineWidth),
                            lineCap: .round))
                .rotationEffect(.degrees(-90))
                .animation(.easeInOut(duration: 1.5))
        }
        .frame(width: CGFloat(frameWidth), height: CGFloat(frameHeight))
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressView(currValue: 5000,
                          goal: 10000,
                          frameWidth: 200,
                          frameHeight: 200,
                          lineWidth: 30,
                          color: Color.pink)
    }
}
