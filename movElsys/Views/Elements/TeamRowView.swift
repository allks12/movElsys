//
//  TeamRowView.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import SwiftUI

struct TeamRowView: View {
    private var team: Team
    private var teamWeekSteps: String
    
    init(team: Team, weekSteps: Int) {
        self.team = team
        self.teamWeekSteps = String(weekSteps)
    }
    
    var body: some View {
        HStack {
            
            TeamLeaguePlaceView(placeInLeague: team.placeIntLeague,
                                frameWidth: 30,
                                frameHeight: 30,
                                lineWidth: 2,
                                numberSize: 15,
                                shadowRadius: 0)
            
            Text(team.name)
                .fontWeight(.bold)
            
            Spacer()
            
            Text(teamWeekSteps)
                .foregroundColor(Color(R.color.darkBlue))
            
            Image(systemName: "shoeprints.fill")
                .foregroundColor(Color(R.color.darkBlue))
            
        }
        .padding(.horizontal)
    }
}

struct TeamRowView_Previews: PreviewProvider {
    static var previews: some View {
        TeamRowView(team: team1, weekSteps: 20511)
    }
}
