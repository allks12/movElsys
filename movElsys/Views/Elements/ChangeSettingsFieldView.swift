//
//  ChangeSettingsFieldView.swift
//  movElsys
//
//  Created by Aleksandra on 5.02.23.
//

import SwiftUI

struct ChangeSettingsFieldView: View {
    @Binding private var content: String
    private let placeholder: String
    @State private var isItNum: Bool
        
    public init(content: Binding<String>, placeholder: String, isItNum: Bool) {
        self._content = content
        self.placeholder = placeholder
        self.isItNum = isItNum
    }
    
    var body: some View {
        TextField(placeholder, text: $content)
            .padding(.all)
            .background(Color(.secondarySystemBackground))
            .cornerRadius(5.0)
            .disableAutocorrection(true)
            .autocapitalization(.none)
            .keyboardType(isItNum ? .numberPad : .default)
    }
}

struct ChangeSettingsFieldView_Previews: PreviewProvider {
    static var previews: some View {
        ChangeSettingsFieldView(content: .constant(""), placeholder: "Text", isItNum: false)
    }
}
