//
//  TeamLeaguePlaceView.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import SwiftUI

struct TeamLeaguePlaceView: View {
    private var placeInLeague: Int
    private var frameWidth: Int
    private var frameHeight: Int
    private var lineWidth: Int
    private var numberSize: Int
    private var shadowRadius: Int
    
    private var leagueColor: Color {
        if placeInLeague == 1 { return Color(R.color.gold) }
        if placeInLeague == 2 { return Color(R.color.silver) }
        if placeInLeague == 3 { return Color(R.color.bronze) }
        return Color(R.color.darkBlue)
    }
    
    init(placeInLeague: Int,
         frameWidth: Int,
         frameHeight: Int,
         lineWidth: Int,
         numberSize: Int,
         shadowRadius: Int) {
        self.placeInLeague = placeInLeague
        self.frameWidth = frameWidth
        self.frameHeight = frameHeight
        self.lineWidth = lineWidth
        self.numberSize = numberSize
        self.shadowRadius = shadowRadius
    }
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: CGFloat(lineWidth))
                .frame(width: CGFloat(frameWidth),
                       height: CGFloat(frameHeight))
                .shadow(color: leagueColor,
                        radius: CGFloat(shadowRadius))
            
            Text(String(placeInLeague))
                .font(.system(size: CGFloat(numberSize)))
                .fontWeight(.heavy)
                .shadow(color: leagueColor,
                        radius: CGFloat(shadowRadius / 2))
        }
        .foregroundColor(leagueColor)
    }
}

struct TeamLeaguePlaceView_Previews: PreviewProvider {
    static var previews: some View {
        TeamLeaguePlaceView(placeInLeague: 1,
                            frameWidth: 80,
                            frameHeight: 80,
                            lineWidth: 7,
                            numberSize: 35,
                            shadowRadius: 10)
    }
}
