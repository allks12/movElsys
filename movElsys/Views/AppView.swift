//
//  AppView.swift
//  movElsys
//
//  Created by Aleks on 23.12.22.
//

import SwiftUI

struct AppView: View {
    @State private var isLoggedIn: Bool = true
    
    @EnvironmentObject private var viewModel: UserViewModel
    private var healthStore: HealthStore
    private var goalsViewModel: GoalsViewModel
    
    init() {
        self.healthStore = HealthStore()
        self.goalsViewModel = GoalsViewModel()
    }
    
    var body: some View {
        if !isLoggedIn {
            WelcomeView()
        } else {
            VStack {
                HeaderView(isLoggedIn: $isLoggedIn)
                    .environmentObject(healthStore)
                    .environmentObject(goalsViewModel)
                
                TabView {
                    HomeScreenView()
                        .environmentObject(healthStore)
                        .environmentObject(goalsViewModel)
                        .tabItem {
                            Image(systemName: "figure.run")
                            Text("Active")
                        }
                    
                    RankingView()
                        .tabItem {
                            Image(systemName: "trophy.fill")
                            Text("Ranking")
                        }
                    
                    HistoryView()
                        .environmentObject(healthStore)
                        .environmentObject(goalsViewModel)
                        .tabItem {
                            Image(systemName: "clock.fill")
                            Text("History")
                        }
                    
                    ProfileSettingsView()
                        .environmentObject(goalsViewModel)
                        .tabItem {
                            Image(systemName: "gearshape.fill")
                            Text("Settings")
                        }
                }
                .accentColor(Color(R.color.darkBlue))
            }
        }
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
