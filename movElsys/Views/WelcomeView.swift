//
//  WelcomeView.swift
//  movElsys
//
//  Created by Aleks on 23.12.22.
//

import SwiftUI
import FirebaseAuth

struct WelcomeView: View {
    @EnvironmentObject private var viewModel: UserViewModel
    
    var body: some View {
        if viewModel.isLoggedIn() {
            AppView()
        } else {
            NewUserView()
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
