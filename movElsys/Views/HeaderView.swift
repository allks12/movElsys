//
//  HeaderView.swift
//  movElsys
//
//  Created by Aleks on 18.12.22.
//

import SwiftUI

struct HeaderView: View {
    @Binding var isLoggedIn: Bool
    @State private var showSheet = false
    
    @EnvironmentObject private var userViewModel: UserViewModel
    @EnvironmentObject private var goalsViewModel: GoalsViewModel
    
    var body: some View {
        HStack {
            Image("logo_blue_no_background")
                .resizable()
                .scaledToFit()
                .frame(width: 70)
            
            Text("MovElsys")
                .fontWeight(.semibold)
            
            Spacer()
            
            Button {
                self.showSheet = true
            } label: {
                Image(systemName: "person.circle.fill")
                    .foregroundColor(Color(R.color.darkBlue))
            }
            .sheet(isPresented: $showSheet) {
                ProfileView(isLoggedIn: $isLoggedIn)
            }
        }
        .padding(.all)
        .font(.title)
    }
}
