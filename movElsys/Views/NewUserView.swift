//
//  NewUserView.swift
//  movElsys
//
//  Created by Aleks on 14.01.23.
//

import SwiftUI

struct NewUserView: View {
    @EnvironmentObject private var viewModel: UserViewModel
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                
                Text("MovElsys")
                    .font(.system(size: 50))
                    .fontWeight(.heavy)
                
                Spacer()
                
                Image("logo_blue_no_background")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    
                Spacer()
                    
                NavigationLink(destination: RegisterView()
                    .navigationBarBackButtonHidden(true)) {
                        ButtonView(text: "Register")
                    }
                
                NavigationLink(destination: LoginView()
                    .navigationBarBackButtonHidden(true)) {
                        ButtonView(text: "Login")
                    }
                
                Spacer()
            }
            .padding(.all)
            .foregroundColor(Color(R.color.darkBlue))
        }
    }
}

struct NewUserView_Previews: PreviewProvider {
    static var previews: some View {
        NewUserView()
    }
}
