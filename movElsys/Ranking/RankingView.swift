//
//  RankingView.swift
//  movElsys
//
//  Created by Aleks on 18.12.22.
//

import SwiftUI

struct RankingView: View {
    private let teams: [Team]
    private let rankingViewModel = RankingViewModel()
    
    init() {
        self.teams = rankingViewModel.getTeams()
    }
    
    var body: some View {
        NavigationView {
            VStack {
                Text("Ranking")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundColor(Color.black)
                    .padding(.vertical)
                
                Spacer()
                
                List(teams, id: \.id) { team in
                    NavigationLink {
                        TeamView(team: team,
                                 rankingViewModel: rankingViewModel)
                    } label: {
                        TeamRowView(team: team,
                                    weekSteps: rankingViewModel.calculateTeamsWeeklySteps(team: team.users))
                    }
                }
            }
        }
    }
}

struct RankingView_Previews: PreviewProvider {
    static var previews: some View {
        RankingView()
    }
}
