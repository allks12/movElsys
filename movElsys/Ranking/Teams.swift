//
//  Teams.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import Foundation

let team1 = Team(id: 1,
                 name: "12 А",
                 users: [user4, user6, user1, user5, user8],
                 placeIntLeague: 1)

let team2 = Team(id: 2,
                 name: "10 Б",
                 users: [user1, user2, user3, user4, user5],
                 placeIntLeague: 2)

let team3 = Team(id: 3,
                 name: "9 Г",
                 users: [user6, user2, user8, user9, user10],
                 placeIntLeague: 3)

let team4 = Team(id: 4,
                 name: "11 В",
                 users: [user7, user3, user9, user2, user1],
                 placeIntLeague: 4)

let team5 = Team(id: 5,
                 name: "8 А",
                 users: [user8, user1, user3, user2, user7],
                 placeIntLeague: 5)

let team6 = Team(id: 6,
                 name: "9 Б",
                 users: [user2, user7, user1, user9, user2],
                 placeIntLeague: 6)
