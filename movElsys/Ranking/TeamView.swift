//
//  TeamView.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import SwiftUI

struct TeamView: View {
    private var teamName: String
    private var placeInLeague: Int
    private var teamUsers: [User]
    private let rankingViewModel: RankingViewModel
    
    private var weeklySteps: Int {
        rankingViewModel.calculateTeamsWeeklySteps(team: teamUsers)
    }
    
    init(team: Team, rankingViewModel: RankingViewModel) {
        self.rankingViewModel = rankingViewModel
        self.teamName = team.name
        self.placeInLeague = team.placeIntLeague
        self.teamUsers = team.users
    }
    
    var body: some View {
        VStack {
            Spacer()
            
            TeamLeaguePlaceView(placeInLeague: placeInLeague,
                                frameWidth: 80,
                                frameHeight: 80,
                                lineWidth: 7,
                                numberSize: 35,
                                shadowRadius: 4)
            
            Text(teamName)
                .font(.largeTitle)
                .fontWeight(.bold)
                .foregroundColor(Color(R.color.darkBlue))
                .padding(.vertical)
                .shadow(radius: 2)
            
            Spacer()
            
            HStack {
                Text("Weekly steps")
                
                Spacer()
                
                Text(String(weeklySteps))
                Image(systemName: "shoeprints.fill")
            }
            .padding(.all)
            .foregroundColor(Color(R.color.darkBlue))
            
            Spacer()
            
            List(teamUsers, id: \.email) {user in
                HStack {
                    Text(user.name)
                    
                    Spacer()
                    
                    Text(String(user.weeklySteps))
                        .foregroundColor(Color(R.color.darkBlue))
                    
                    Image(systemName: "shoeprints.fill")
                        .foregroundColor(Color(R.color.darkBlue))
                }
            }
        }
    }
}

struct TeamView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = RankingViewModel()
        
        TeamView(team: team1, rankingViewModel: viewModel)
    }
}
