//
//  Users.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import Foundation

let user1 = User(name: "Aleksandra",
                 email: "allks@aa.aa",
                 password: "Pass123",
                 weeklySteps: 17789)

let user2 = User(name: "Nikola",
                 email: "nikola@pp.pp",
                 password: "Pass123",
                 weeklySteps: 14629)

let user3 = User(name: "Ivana",
                 email: "ivvana@aa.aa",
                 password: "Pass123",
                 weeklySteps: 22378)

let user4 = User(name: "Anton",
                 email: "anton@aa.aa",
                 password: "Pass123",
                 weeklySteps: 17556)

let user5 = User(name: "Polina",
                 email: "poli@aa.aa",
                 password: "Pass123",
                 weeklySteps: 33004)

let user6 = User(name: "Peter",
                 email: "peter@aa.aa",
                 password: "Pass123",
                 weeklySteps: 45229)

let user7 = User(name: "Nadia",
                 email: "nadia@pp.pp",
                 password: "Pass123",
                 weeklySteps: 10449)

let user8 = User(name: "Vladislav",
                 email: "vladi@aa.aa",
                 password: "Pass123",
                 weeklySteps: 20378)

let user9 = User(name: "Elena",
                 email: "elena@aa.aa",
                 password: "Pass123",
                 weeklySteps: 1252)

let user10 = User(name: "Victoria",
                 email: "viki@aa.aa",
                 password: "Pass123",
                 weeklySteps: 9801)
