//
//  TeamModel.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import Foundation

struct Team {
    let id: Int
    let name: String
    let users: [User]
    let placeIntLeague: Int
}
