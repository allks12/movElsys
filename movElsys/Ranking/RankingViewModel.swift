//
//  RankingViewModel.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import Foundation

class RankingViewModel {
    private let teams: [Team] = [team1, team2, team3, team4, team5, team6]
    
    func calculateTeamsWeeklySteps(team: [User]) -> Int {
        var weeklySteps = 0
        
        for user in team {
            weeklySteps += user.weeklySteps
        }
        
        return weeklySteps
    }
    
    func getTeams() -> [Team] {
        teams
    }
}
