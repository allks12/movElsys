//
//  HistoryViewModel.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import Foundation

class HistoryViewModel {
    private var endDate: Date? {
        guard let date = Calendar.current.date(byAdding: .day, value: -1, to: Date()) else {
            return nil
        }
        return date
    }
    
    private var startDate: Date? {
        guard let date = Calendar.current.date(byAdding: .month, value: -1, to: Date()) else {
            return nil
        }
        return date
    }
    
    func updateStepValues(healthStore: HealthStore, completion: @escaping (Result<[DailyStepsData], Error>) -> Void) {
        healthStore.requestAuthorization { success in
            switch success {
            case .success(_):
                healthStore.calculateDailyStepsForTheLastMont { calculationSuccess in
                    switch calculationSuccess {
                    case .success(let statisticsCollection):
                        if let startDay = self.startDate, let endDay = self.endDate {
                            var monthlySteps = [DailyStepsData]()
                            statisticsCollection.enumerateStatistics(from: startDay,
                                                                     to: endDay) { statistics, _ in
                                
                                let count = statistics.sumQuantity()?.doubleValue(for: .count())
                                let steps = DailyStepsData(count: Int(count ?? 0),
                                                           date: statistics.startDate)
                                monthlySteps.append(steps)
                            }
                            completion(.success(monthlySteps))
                        } else {
                            completion(.failure(Errors.historyStepsError))
                        }
                        
                    case .failure(let error):
                        completion(.failure(error))
                    }
                    
                }
            case .failure(_):
                completion(.failure(Errors.historyStepsError))
            }
        }
    }
    
    func updateEnergyValues(healthStore: HealthStore,
                            completion: @escaping (Result<[DailyEnergyData], Error>) -> Void) {
        healthStore.requestAuthorization { success in
            switch success {
            case .success(_):
                healthStore.calculateDailyEnergyForTheLastMonth { calculationSuccess in
                    switch calculationSuccess {
                    case .success(let statisticsCollection):
                        if let startDay = self.startDate, let endDay = self.endDate {
                            var monthlyEnergy = [DailyEnergyData]()
                            statisticsCollection.enumerateStatistics(from: startDay,
                                                                     to: endDay) { statistics, _ in
                                
                                let energy = statistics.sumQuantity()?.doubleValue(for: .kilocalorie())
                                let burnedEnergy = DailyEnergyData(kcal: Int(energy ?? 0),
                                                                   date: statistics.startDate)
                                monthlyEnergy.append(burnedEnergy)
                            }
                            completion(.success(monthlyEnergy))
                        } else {
                            completion(.failure(Errors.historyEnergyError))
                        }
                    case .failure(let error):
                        completion(.failure(error))
                    }
                    
                }
            case .failure(_):
                completion(.failure(Errors.historyEnergyError))
            }
        }
    }
}
