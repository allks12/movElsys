//
//  HistoryView.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import SwiftUI

struct HistoryView: View {
    @EnvironmentObject private var healthStore: HealthStore
    @EnvironmentObject private var goalsViewModel: GoalsViewModel
    
    @State private var monthStepsData = [DailyStepsData]()
    @State private var monthEnergyData = [DailyEnergyData]()
    @State private var stepsErrorOccured = false
    @State private var energyErrorOccured = false
    @State private var currDailyStepsGoal = 0
    @State private var currDailyEnergyGoal = 0
    
    private let historyViewModel: HistoryViewModel
    private let dateFormatter: DateFormatter
    
    init() {
        historyViewModel = HistoryViewModel()
        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = "MMM dd"
    }
    
    var body: some View {
        VStack {
            Text("History")
                .font(.largeTitle)
                .fontWeight(.bold)
                .foregroundColor(.black)
            
            if stepsErrorOccured {
                Text(ErrorMessage.historyStepsError.rawValue)
                    .foregroundColor(.red)
                    .font(.footnote)
                    .padding(.vertical)
            }
            
            if energyErrorOccured {
                Text(ErrorMessage.historyEnergyError.rawValue)
                    .foregroundColor(.red)
                    .font(.footnote)
                    .padding(.bottom)
            }
            
            Spacer()
            
            List(monthStepsData.reversed(), id: \.id) { step in
                HStack {
                    VStack {
                        Spacer()
                        
                        Text(dateFormatter.string(from: step.date))
                            .foregroundColor(.black.opacity(0.5))
                        
                        Spacer()
                    }
                    
                    Spacer()
                    
                    VStack {
                        Image(systemName: "shoeprints.fill")
                            .foregroundColor(Color(R.color.darkBlue))
                            .padding(.bottom, 10)
                        
                        Image(systemName: "flame.fill")
                            .foregroundColor(.pink)
                    }
                    .padding(.horizontal)
                    
                    VStack {
                        Text(String(step.count))
                            .foregroundColor(Color(R.color.darkBlue))
                            .padding(.bottom, 3)
                        
                        Text(String(self.filterEnergy(date: step.date).kcal))
                            .foregroundColor(.pink)
                    }
                    
                    Spacer()
                    
                    VStack {
                        TargetProgressView(currValue: step.count,
                                           goal: currDailyStepsGoal != 0 ? currDailyStepsGoal
                                           : goalsViewModel.getStepsGoal())
                            .padding(.bottom, 10)
                        
                        TargetProgressView(currValue: self.filterEnergy(date: step.date).kcal,
                                           goal: currDailyEnergyGoal != 0 ? currDailyEnergyGoal
                                           : goalsViewModel.getEnergyGoal())
                    }
                }
            }
            .onAppear {
                currDailyStepsGoal = goalsViewModel.getStepsGoal()
                currDailyEnergyGoal = goalsViewModel.getEnergyGoal()
                
                updateStepsInfoUI()
                updateEnergyInfoUI()
            }
        }
    }
    
    func updateStepsInfoUI() {
        historyViewModel.updateStepValues(healthStore: healthStore) { success in
            switch success {
            case .success(let data):
                monthStepsData = data
            case .failure(let error):
                print(error)
                stepsErrorOccured = true
            }
        }
    }
    
    func updateEnergyInfoUI() {
        historyViewModel.updateEnergyValues(healthStore: healthStore) { success in
            switch success {
            case .success(let data):
                monthEnergyData = data
            case .failure(let error):
                print(error)
                energyErrorOccured = true
            }
        }
    }
    
    func filterEnergy(date: Date) -> DailyEnergyData {
        monthEnergyData.filter { data in
            return data.date == date
        }.first ?? DailyEnergyData(kcal: 0, date: date)
    }

}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        HistoryView()
    }
}
