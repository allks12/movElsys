//
//  HealthStore.swift
//  movElsys
//
//  Created by Aleksandra on 29.01.23.
//

import Foundation
import HealthKit

class HealthStore: ObservableObject {
    private var healthStore: HKHealthStore?
    private let stepType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
    private let energyType = HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!
    
    init() {
        if HKHealthStore.isHealthDataAvailable() {
            healthStore = HKHealthStore()
        }
    }
    
    func requestAuthorization(completion: @escaping (Result<Bool, Error>) -> Void) {
        guard let healthStore = self.healthStore else {
            completion(.failure(Errors.unathorizedHealthDataAccess))
            return
        }
        
        healthStore.requestAuthorization(toShare: [], read: [stepType, energyType]) { success, error in
            if error != nil {
                completion(.failure(Errors.unathorizedHealthDataAccess))
            }
            if success {
                completion(.success(true))
            }
        }
    }
    
    func calculateSteps(completion: @escaping (Result<Int, Error>) -> Void) {
        let now = Date.now
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay,
                                                    end: now,
                                                    options: .strictStartDate)
        let observerQuery = HKObserverQuery(sampleType: stepType,
                                            predicate: predicate) {query, completionHandler, error in
            
            if let error = error {
                completion(.failure(Errors.dailyStepsError))
                print(error.localizedDescription)
                return
            }
            
            let query = HKStatisticsQuery(quantityType: self.stepType,
                                          quantitySamplePredicate: predicate,
                                          options: .cumulativeSum) { _, result, error in
                
                if let error = error {
                    completion(.failure(Errors.dailyStepsError))
                    print(error.localizedDescription)
                    return
                }
                completion(.success(Int(result!.sumQuantity()!.doubleValue(for: .count()))))
            }
            self.healthStore?.execute(query)
            completionHandler()
        }
        healthStore?.execute(observerQuery)
    }
    
    func calculateBurnedEnergy(completion: @escaping (Result<Int, Error>) -> Void) {
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay,
                                                    end: now,
                                                    options: .strictStartDate)
        let query = HKStatisticsQuery(quantityType: energyType,
                                      quantitySamplePredicate: predicate,
                                      options: .cumulativeSum) { _, result, _ in
            guard let result = result, let sum = result.sumQuantity() else {
                completion(.failure(Errors.burnedEnergyError))
                return
            }
            completion(.success(Int(sum.doubleValue(for: .kilocalorie()))))
        }
        healthStore?.execute(query)
    }
    
    func calculateWeekSteps(completion: @escaping (Result<Int, Error>) -> Void) {
        let now = Date()
        let components = Calendar.current.dateComponents([.yearForWeekOfYear,
                                                          .weekOfYear],
                                                         from: now)
        let startOfWeek = Calendar.current.date(from: components)
        let predicate = HKQuery.predicateForSamples(withStart: startOfWeek,
                                                    end: now,
                                                    options: .strictStartDate)
        let query = HKStatisticsQuery(quantityType: stepType,
                                      quantitySamplePredicate: predicate,
                                      options: .cumulativeSum) { _, result, _ in
            guard let result = result, let sum = result.sumQuantity() else {
                completion(.failure(Errors.weekStepsError))
                return
            }
            completion(.success(Int(sum.doubleValue(for: .count()))))
        }
        healthStore?.execute(query)
    }
    
    func calculateMonthSteps(completion: @escaping (Result<Int, Error>) -> Void) {
        let now = Date()
        let components = Calendar.current.dateComponents([.year,
                                                          .month],
                                                         from: now)
        let startOfMonth = Calendar.current.date(from: components)
        let predicate = HKQuery.predicateForSamples(withStart: startOfMonth,
                                                    end: now,
                                                    options: .strictStartDate)
        let query = HKStatisticsQuery(quantityType: stepType,
                                      quantitySamplePredicate: predicate,
                                      options: .cumulativeSum) { _, result, _ in
            guard let result = result, let sum = result.sumQuantity() else {
                completion(.failure(Errors.monthStepsError))
                return
            }
            completion(.success(Int(sum.doubleValue(for: .count()))))
        }
        healthStore?.execute(query)
    }
    
    func calculateDailyStepsForTheLastMont(completion: @escaping (Result<HKStatisticsCollection, Error>) -> Void) {
        let now = Date()
        let startDay = Calendar.current.date(byAdding: .month, value: -1, to: now)
        let startOfDay = Calendar.current.startOfDay(for: now)
        let daily = DateComponents(day: 1)

        let predicate = HKQuery.predicateForSamples(withStart: startDay,
                                                    end: now,
                                                    options: .strictStartDate)

        let stepQuery = HKStatisticsCollectionQuery(quantityType: stepType,
                                                quantitySamplePredicate: predicate,
                                                options: .cumulativeSum,
                                                anchorDate: startOfDay,
                                                intervalComponents: daily)

        stepQuery.initialResultsHandler = { _, statisticsCollection, error in
            if error != nil {
                completion(.failure(Errors.historyStepsError))
            }
            
            if let statisticsCollection = statisticsCollection {
                completion(.success(statisticsCollection))
            } else {
                completion(.failure(Errors.historyStepsError))
            }
        }

        healthStore?.execute(stepQuery)
    }
    
    func calculateDailyEnergyForTheLastMonth(completion: @escaping (Result<HKStatisticsCollection, Error>) -> Void) {
        let now = Date()
        let startDay = Calendar.current.date(byAdding: .month, value: -1, to: now)
        let startOfDay = Calendar.current.startOfDay(for: now)
        let daily = DateComponents(day: 1)

        let predicate = HKQuery.predicateForSamples(withStart: startDay,
                                                    end: now,
                                                    options: .strictStartDate)

        let stepQuery = HKStatisticsCollectionQuery(quantityType: energyType,
                                                    quantitySamplePredicate: predicate,
                                                    options: .cumulativeSum,
                                                    anchorDate: startOfDay,
                                                    intervalComponents: daily)

        stepQuery.initialResultsHandler = { _, statisticsCollection, error in
            if error != nil {
                completion(.failure(Errors.historyEnergyError))
            }
            
            if let statisticsCollection = statisticsCollection {
                completion(.success(statisticsCollection))
            } else {
                completion(.failure(Errors.historyStepsError))
            }
        }

        healthStore?.execute(stepQuery)
    }
    
}
