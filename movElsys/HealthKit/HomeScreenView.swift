//
//  HomeScreenView.swift
//  movElsys
//
//  Created by Aleks on 15.12.22.
//

import SwiftUI

struct HomeScreenView: View {
    @State private var todaySteps = 0
    @State private var todayEnergyBurned = 0
    @State private var currWeeklySteps = 0
    @State private var currMonthlySteps = 0
    @State private var errorOccured = false
    @State private var errorDailyDataOccured = false
    @State private var errorWeekMonthStepsOccured = false
    
    @EnvironmentObject private var healthStore: HealthStore
    @EnvironmentObject private var goalsViewModel: GoalsViewModel
    
    @State private var currDailyStepsGoal = 0
    @State private var currDailyEnergyGoal = 0
    
    private var weeklyStepsGoal: Int {
        goalsViewModel.getStepsGoal() * 7
    }
    
    private var monthlyStepsGoal: Int? {
        guard let range = Calendar.current.range(of: .day, in: .month, for: Date()) else {
            return nil
        }
        let numberOfDays = range.count
        
        return goalsViewModel.getStepsGoal() * numberOfDays
    }
    
    var body: some View {
        VStack {
            if errorOccured {
                Text(ErrorMessage.healthDataAuthFailed.rawValue)
                    .foregroundColor(.red)
                    .font(.footnote)
                    .padding(.bottom)
            }
            
            if errorDailyDataOccured {
                Text(ErrorMessage.dailyDataError.rawValue)
                    .foregroundColor(.red)
                    .font(.footnote)
                    .padding(.bottom)
            }

            if errorWeekMonthStepsOccured {
                Text(ErrorMessage.weekMonthStepsError.rawValue)
                    .foregroundColor(.red)
                    .font(.footnote)
                    .padding(.bottom)
            }
            
            Text("Today")
                .font(.largeTitle)
                .fontWeight(.bold)
                .foregroundColor(Color.black)
                .padding(.vertical)
            
            Spacer()
            
            ZStack {
                
                ProgressView(currValue: todaySteps,
                            goal: currDailyStepsGoal != 0 ? currDailyStepsGoal : goalsViewModel.getStepsGoal(),
                            frameWidth: 250,
                            frameHeight: 250,
                            lineWidth: 30,
                            color: Color(R.color.darkBlue))
                                  
                ProgressView(currValue: todayEnergyBurned,
                            goal: currDailyEnergyGoal != 0 ? currDailyEnergyGoal : goalsViewModel.getEnergyGoal(),
                            frameWidth: 190,
                            frameHeight: 190,
                            lineWidth: 30,
                            color: Color.pink)
                
                VStack {
                    HStack {
                        Image(systemName: "shoeprints.fill")
                            .font(.system(size: 40))
                        
                        Text(String(todaySteps))
                            .font(.title)
                            .fontWeight(.heavy)
                    }
                    
                    HStack {
                        Image(systemName: "flame.fill")
                            .font(.system(size: 20))
                        
                        Text(String(todayEnergyBurned))
                            .font(.title3)
                            .fontWeight(.heavy)
                    }
                    .foregroundColor(Color.pink)
                    .padding(.top)
                }
                
            }
                    
            Spacer()
            
            HStack {
                VStack {
                    ZStack {
                        
                        ProgressView(currValue: currWeeklySteps,
                                          goal: weeklyStepsGoal,
                                          frameWidth: 100,
                                          frameHeight: 100,
                                          lineWidth: 20,
                                          color: Color(R.color.darkBlue))
                        
                        Text(String(currWeeklySteps))
                            .fontWeight(.heavy)
                            .font(.subheadline)
                    }
                    .padding(.all)
                    
                    Text("This week")
                        .fontWeight(.heavy)
                }
                
                Spacer()
                
                VStack {
                    ZStack {
                        if let monthlyStepsGoal = self.monthlyStepsGoal {
                            ProgressView(currValue: currMonthlySteps,
                                         goal: monthlyStepsGoal,
                                         frameWidth: 100,
                                         frameHeight: 100,
                                         lineWidth: 20,
                                         color: Color(R.color.darkBlue))
                            
                            Text(String(currMonthlySteps))
                                .fontWeight(.heavy)
                                .font(.subheadline)
                        }
                    }
                    .padding(.all)
                    
                    Text("This month")
                        .fontWeight(.heavy)
                }
                
            }
            .padding(.all)
            
            Spacer()
        }
        .foregroundColor(Color(R.color.darkBlue))
        .onAppear {
            currDailyStepsGoal = goalsViewModel.getStepsGoal()
            currDailyEnergyGoal = goalsViewModel.getEnergyGoal()
            
            updateDailyStepsEnergy()
            updateWeeklyMonthlySteps()
            
            Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { _ in
                updateDailyStepsEnergy()
            }
        }
    }
    
    func updateDailyStepsEnergy() {
        healthStore.requestAuthorization { success in
            switch success {
            case .success(_):
                errorOccured = false
                healthStore.calculateSteps { dailyStepsSuccess in
                    switch dailyStepsSuccess {
                    case .success(let steps):
                        todaySteps = steps
                        errorDailyDataOccured = false
                    case .failure(_):
                        errorDailyDataOccured = true
                    }
                }
                healthStore.calculateBurnedEnergy { energySuccess in
                    switch energySuccess {
                    case .success(let energy):
                        todayEnergyBurned = energy
                        errorDailyDataOccured = false
                    case .failure(_):
                        todayEnergyBurned = 0
                        errorDailyDataOccured = true
                    }
                }
            case .failure(_):
                errorOccured = true
            }
        }
    }
    
    func updateWeeklyMonthlySteps() {
        healthStore.requestAuthorization { success in
            switch success {
            case .success(_):
                errorOccured = false
                healthStore.calculateWeekSteps { weekStepsSuccess in
                    switch weekStepsSuccess {
                    case .success(let steps):
                        currWeeklySteps = steps
                        errorWeekMonthStepsOccured = false
                    case . failure(_):
                        errorWeekMonthStepsOccured = true
                    }
                }
                healthStore.calculateMonthSteps { monthStepsSuccess in
                    switch monthStepsSuccess {
                    case .success(let steps):
                        currMonthlySteps = steps
                        errorWeekMonthStepsOccured = false
                    case .failure(_):
                        errorWeekMonthStepsOccured = true
                    }
                }
            case .failure(_):
                errorOccured = true
            }
        }
    }
    
}

struct HomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        HomeScreenView()
    }
}
