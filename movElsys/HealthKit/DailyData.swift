//
//  DailyData.swift
//  movElsys
//
//  Created by Aleksandra on 4.02.23.
//

import Foundation

struct DailyStepsData: Identifiable {
    let id = UUID()
    let count: Int
    let date: Date
}

struct DailyEnergyData {
    let kcal: Int
    let date: Date
}
