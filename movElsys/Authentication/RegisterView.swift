//
//  RegisterView.swift
//  movElsys
//
//  Created by Aleks on 23.12.22.
//

import SwiftUI
import FirebaseAuth

struct RegisterView: View {
    @State private var name = ""
    @State private var email = ""
    @State private var password = ""
    @State private var password2 = ""
    @State private var passwordErrorMessage = ""
    @State private var secPasswordErrorMessage = ""
    @State private var emailErrorMessage = ""
    @State private var registerErrorMessage = ""
    @State private var isLoggedIn = false
    
    private var isEmailValid: Bool {
        viewModel.shouldShowEmailErrorMessage(email: email) || email.isEmpty
    }
    
    private var isPassValid: Bool {
        viewModel.shouldShowPassErrorMessage(password: password) || password.isEmpty
    }
    
    private var isSecPassValid: Bool {
        viewModel.shouldShowSecPassErrorMessage(pass1: password, pass2: password2)
            || password2.isEmpty
    }
    
    @EnvironmentObject private var viewModel: UserViewModel
    private var fieldValidator = FieldValidator()
    
    var body: some View {
        if isLoggedIn {
            AppView()
        } else {
            VStack {
                Text("Register")
                    .font(.system(size: 50))
                    .fontWeight(.heavy)
                    .padding(.top)
                
                Spacer()
                
                InputTextFieldView(content: $name, placeholder: "Name")
                    .overlay(RoundedRectangle(cornerRadius: 1)
                        .stroke(Color(R.color.darkBlue), lineWidth: 1))
                    .padding(.bottom, 20)
                
                Group {
                    InputTextFieldView(content: $email, placeholder: "Email")
                        .onChange(of: email) { _ in
                            if !isEmailValid {
                                emailErrorMessage = ErrorMessage.invalidEmail.rawValue
                            }
                        }
                        .overlay(RoundedRectangle(cornerRadius: 1)
                            .stroke(!isEmailValid ? .red : Color(R.color.darkBlue), lineWidth: 1))
                    
                    if !isEmailValid {
                        Text(emailErrorMessage)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                }
                .padding(.bottom, 20)
                
                Group {
                    InputPasswordView(content: $password, placeholder: "Password")
                        .onChange(of: password) { _ in
                            if !isPassValid {
                                passwordErrorMessage = ErrorMessage.tooShortPass.rawValue
                            }
                        }
                        .overlay(RoundedRectangle(cornerRadius: 1)
                            .stroke(!isPassValid ? .red : Color(R.color.darkBlue), lineWidth: 1))
                    
                    if !isPassValid {
                        Text(passwordErrorMessage)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                    
                    InputPasswordView(content: $password2, placeholder: "Repeat password")
                        .onChange(of: password2) { _ in
                            if !isSecPassValid {
                                secPasswordErrorMessage = ErrorMessage.passwordsNotMatch.rawValue
                            }
                        }
                        .overlay(RoundedRectangle(cornerRadius: 1)
                            .stroke(!isSecPassValid ? .red : Color(R.color.darkBlue), lineWidth: 1))
                    
                    if !isSecPassValid {
                        Text(secPasswordErrorMessage)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                }
                .padding(.bottom, 20)
                
                if !registerErrorMessage.isEmpty {
                    Text(registerErrorMessage)
                        .foregroundColor(.red)
                        .font(.footnote)
                }
                
                Spacer()
                
                Button {
                    viewModel.register(email: email, password: password,
                                       name: name) { success in
                        switch success {
                        case .success():
                            isLoggedIn = true
                        case .failure(_):
                            email = ""
                            password = ""
                            password2 = ""
                            name = ""
                            registerErrorMessage = ErrorMessage.takenEmail.rawValue
                        }
                    }
                } label: {
                    ButtonView(text: "Register")
                }
                .disabled(!fieldValidator
                    .isRegisterFormReadyForSubmission(email: email, password: password, pass2: password2,
                                                      name: name))
                .foregroundColor(!fieldValidator
                    .isRegisterFormReadyForSubmission(email: email, password: password, pass2: password2,
                                                      name: name)
                                 ? .gray : Color(R.color.darkBlue))
                
                NavigationLink("Already have an account? Log in",
                               destination: LoginView().navigationBarBackButtonHidden(true))
                .padding(.bottom)
            }
            .padding(.all)
            .foregroundColor(Color(R.color.darkBlue))
            .onTapGesture {
                hideKeyboard()
            }
        }
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
