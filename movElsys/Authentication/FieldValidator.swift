//
//  EmailCheck.swift
//  movElsys
//
//  Created by Aleks on 5.01.23.
//

import Foundation

public class FieldValidator {
    func emailContentValidation(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: email)
    }
    
    func validateEmail(_ email: String) -> Bool {
        emailContentValidation(email: email) && !email.isEmpty
    }
    
    func validateName(_ name: String) -> Bool {
        !name.isEmpty
    }
    
    func validatePassword(_ password: String) -> Bool {
        password.count >= 6
    }
    
    func isRegisterFormReadyForSubmission(email: String, password: String, pass2: String,
                                          name: String) -> Bool {
        validateEmail(email) && validatePassword(password)
                && validateName(name)
                && areTwoPasswordsTheSame(pass1: password, pass2: pass2)
    }
    
    func isLoginFormReadyForSubmission(email: String, password: String) -> Bool {
        validateEmail(email) && validatePassword(password)
    }
    
    func areTwoPasswordsTheSame(pass1: String, pass2: String) -> Bool {
        pass1.elementsEqual(pass2)
    }
}
