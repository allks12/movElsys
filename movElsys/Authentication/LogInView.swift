//
//  LoginView.swift
//  movElsys
//
//  Created by Aleks on 23.12.22.
//

import SwiftUI

struct LoginView: View {
    @State private var email = ""
    @State private var password = ""
    @State private var passwordErrorMessage = ""
    @State private var emailErrorMessage = ""
    @State private var loginErrorMessage = ""
    @State private var isLoggedIn = false
    
    private var isEmailValid: Bool {
        viewModel.shouldShowEmailErrorMessage(email: email) || email.isEmpty
    }
    
    private var isPassValid: Bool {
        viewModel.shouldShowPassErrorMessage(password: password) || password.isEmpty
    }
    
    @EnvironmentObject private var viewModel: UserViewModel
    private var fieldValidator = FieldValidator()
    
    var body: some View {
        if isLoggedIn {
            AppView()
        } else {
            VStack {
                Text("Log In")
                    .font(.system(size: 50))
                    .fontWeight(.heavy)
                    .padding(.top)
                
                Spacer()
                
                Group {
                    InputTextFieldView(content: $email, placeholder: "Email")
                    .onChange(of: email) { _ in
                         if !isEmailValid {
                             emailErrorMessage = ErrorMessage.invalidEmail.rawValue
                         }
                     }
                        .overlay(RoundedRectangle(cornerRadius: 1)
                            .stroke(!isEmailValid ? .red
                                    : Color(R.color.darkBlue), lineWidth: 1))
                    
                    if !isEmailValid {
                        Text(emailErrorMessage)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                }
                .padding(.bottom, 20)
                
                Group {
                    InputPasswordView(content: $password, placeholder: "Password")
                    .onChange(of: password) { _ in
                         if !isPassValid {
                             passwordErrorMessage = ErrorMessage.tooShortPass.rawValue
                         }
                     }
                        .overlay(RoundedRectangle(cornerRadius: 1)
                            .stroke(!isPassValid ? .red
                                    : Color(R.color.darkBlue), lineWidth: 1))
                    
                    if !isPassValid {
                        Text(passwordErrorMessage)
                            .foregroundColor(.red)
                            .font(.footnote)
                    }
                }
                .padding(.bottom, 20)
                
                if !loginErrorMessage.isEmpty {
                    Text(loginErrorMessage)
                        .foregroundColor(.red)
                        .font(.footnote)
                }
                
                Spacer()
                
                Button {
                    viewModel.login(email: email, password: password) { success in
                        switch success {
                        case .success():
                            isLoggedIn = true
                        case .failure(_):
                            email = ""
                            password = ""
                            loginErrorMessage =
                                ErrorMessage.wrongEmailOrPass.rawValue
                        }
                    }
                } label: {
                    ButtonView(text: "Login")
                }
                .disabled(!fieldValidator
                 .isLoginFormReadyForSubmission(email: email, password: password))
                .foregroundColor(!fieldValidator
                    .isLoginFormReadyForSubmission(email: email, password: password) ? .gray : Color(R.color.darkBlue))
                
                NavigationLink("Don't have an account? Register",
                               destination: RegisterView().navigationBarBackButtonHidden(true))
                .padding(.bottom)
            }
            .padding(.all)
            .foregroundColor(Color(R.color.darkBlue))
            .onTapGesture {
                hideKeyboard()
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
