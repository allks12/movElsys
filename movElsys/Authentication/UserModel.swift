//
//  UserModel.swift
//  movElsys
//
//  Created by Aleks on 5.01.23.
//

import Foundation
import FirebaseAuth

struct User {
    let name: String
    let email: String
    let password: String
    let weeklySteps: Int
}
