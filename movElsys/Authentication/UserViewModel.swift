//
//  UserViewModel.swift
//  movElsys
//
//  Created by Aleks on 26.12.22.
//

import SwiftUI
import FirebaseAuth

class UserViewModel: ObservableObject {
    private let auth = Auth.auth()
    private var fieldValidator = FieldValidator()
    
    func isLoggedIn() -> Bool {
        return auth.currentUser != nil
    }
    
    func login(email: String, password: String, completion: @escaping (Result<Void, Error>) -> Void) {
        auth.signIn(withEmail: email, password: password) { result, error in
            guard result != nil, error == nil else {
                completion(.failure(Errors.loginError))
                return
            }
            DispatchQueue.main.async {
                completion(.success(()))
            }
        }
    }
    
    func register(email: String, password: String, name: String,
                  completion: @escaping (Result<Void, Error>) -> Void) {
        let user = User(name: name,
                        email: email,
                        password: password,
                        weeklySteps: 0)
        auth.createUser(withEmail: user.email, password: user.password) { result, error in
            guard result != nil, error == nil else {
                completion(.failure(Errors.registerError))
                return
            }
            self.updateUserName(userName: name) { success in
                switch success {
                case .success():
                    break
                case .failure(_):
                    completion(.failure(Errors.registerError))
                }
            }
            DispatchQueue.main.async {
                completion(.success(()))
            }
        }
    }
    
    func logout (completion: @escaping (Result<Bool, Error>) -> Void) {
        do {
            try auth.signOut()
        } catch {
            print("Could not log out!")
            completion(.failure(Errors.loginError))
        }
        completion(.success(true))
    }
    
    func shouldShowEmailErrorMessage(email: String) -> Bool {
        fieldValidator.validateEmail(email)
    }
    
    func shouldShowPassErrorMessage(password: String) -> Bool {
        fieldValidator.validatePassword(password)
    }
    
    func shouldShowSecPassErrorMessage(pass1: String, pass2: String) -> Bool {
        fieldValidator.areTwoPasswordsTheSame(pass1: pass1, pass2: pass2)
    }
    
    func shouldShowNameErrorMessage(name: String) -> Bool {
        fieldValidator.validateName(name)
    }
    
    func updateEmail(newEmail: String, password: String,
                     completion: @escaping (Result<Void, Error>) -> Void) {
        guard let user = auth.currentUser, let email = user.email else {
            completion(.failure(Errors.updateEmailError))
            return
        }
        
        let credentials = EmailAuthProvider.credential(withEmail: email, password: password)
        
        user.reauthenticate(with: credentials) { error, _  in
            guard error != nil else {
                completion(.failure(Errors.updateEmailError))
                return
            }
            
            user.updateEmail(to: newEmail) { error in
                if error != nil {
                    completion(.failure(Errors.updateEmailError))
                    return
                }
                completion(.success(()))
            }
        }
    }
    
    func updatePassword(newPassword: String, currPassword: String,
                        completion: @escaping (Result<Void, Error>) -> Void) {
        guard let user = auth.currentUser, let email = user.email else {
            completion(.failure(Errors.updatePassword))
            return
        }
        
        let credentials = EmailAuthProvider.credential(withEmail: email, password: currPassword)
        
        user.reauthenticate(with: credentials) { error, _ in
            guard error != nil else {
                completion(.failure(Errors.updatePassword))
                return
            }
            
            user.updatePassword(to: newPassword) { error in
                if error != nil {
                    completion(.failure(Errors.updatePassword))
                    return
                }
                completion(.success(()))
            }
        }
    }
    
    func updateUserName(userName: String, completion: @escaping (Result<Void, Error>) -> Void) {
        guard let user = auth.currentUser else {
            completion(.failure(Errors.updateUserNameError))
            return
        }
        
        let changeRequest = user.createProfileChangeRequest()
        changeRequest.displayName = userName
        
        changeRequest.commitChanges { error in
            if error != nil {
                completion(.failure(Errors.updateUserNameError))
                return
            }
            completion(.success(()))
        }
    }
    
    func getUserName(completion: @escaping (Result<String, Error>) -> Void) {
        guard let user = auth.currentUser else {
            completion(.failure(Errors.getUserNameError))
            return
        }
        
        guard let userName = user.displayName else {
            completion(.failure(Errors.getUserNameError))
            return
        }
        
        completion(.success(userName))
    }
}
