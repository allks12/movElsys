//
//  movElsysApp.swift
//  movElsys
//
//  Created by Aleks on 15.12.22.
//

import SwiftUI
import Firebase

@main
struct MovElsysApp: App {
    private let viewModel: UserViewModel
    
    init() {
        FirebaseApp.configure()
        viewModel = UserViewModel()
    }

    var body: some Scene {
        WindowGroup {
            WelcomeView()
                .environmentObject(viewModel)
        }
    }
}
